package jsoup.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class XmlFileReader {
    public static String getFile(String pathToFile) {
        byte[] encoded = new byte[0];
        try {
            encoded = Files.readAllBytes(Paths.get(pathToFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new String(encoded);
    }
}
