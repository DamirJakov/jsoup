package jsoup.test;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello World!");
        String fileContent = XmlFileReader.getFile("test.xml");

        Document document = Jsoup.parse(fileContent);

        Element elementById = document.getElementById("2");

        String text = elementById.text();
        System.out.println("text: " + text);

        Elements stadt = document.getElementsByTag("stadt");
//        Elements stadt2 = document.getElementsByTag("Stadt");

        String text1 = stadt.text();

        String substring = text.substring(2, 5);
        System.out.println("text1: "+text1);
        System.out.println("substring: "+substring);

        System.out.println(stadt);

        String attr = stadt.attr("test");
        System.out.println("attr: "+attr);

        String aaa = attr.replaceAll(" ", "");
        System.out.println("replaceAll: "+aaa);

        String s = aaa.toUpperCase();
        System.out.println("toUpperCase: "+s);

        Elements atrybuty = document.getElementsByTag("atrybuty");
        String a2 = atrybuty.attr("a2");
        String a3 = atrybuty.attr("a3");
        System.out.println(a2);
        System.out.println(a3);



    }
}
